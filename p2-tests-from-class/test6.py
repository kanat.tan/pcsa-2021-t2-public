import socket as sk
from time import sleep

def test_1():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('192.168.1.11', 8080))
        s.endall(b'GET /index.html HTTP/1.1\r\nHost: cs.muic.mahidol.ac.th\r\nConnection: close\r\n\r\n')
        while data := s.recv(1024):
            print(data)

def test_2():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:

        s.connect(('cs.muic.mahidol.ac.th', 80))
        s.sendall(b'GET ')
        sleep(1)
        s.sendall(b'/ HTTP/1.1\r\n')
        sleep(0.5)
        s.sendall(b'Host: cs.muic.mahidol.ac.th\r\nConnection: close\r\n')
        sleep(5)
        s.sendall(b'\r\n')

        while data := s.recv(1024):
            print(data)
            
def test_3():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('192.168.1.11', 8080))
        s.endall(b'HEAD /index.html HTTP/1.1\r\nHost: cs.muic.mahidol.ac.th\r\nConnection: close\r\n\r\n')
        while data := s.recv(1024):
            print(data)
            
def test_4():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('192.168.1.11', 8080))
        s.endall(b'GET /index.html HTTP/1.1\r\nHost: cs.muic.mahidol.ac.th\r\nConnection: close\r\n\r\n')
        while data := s.recv(1024):
            print(data)

def test_5():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th',80))
        s.sendall(b'G')
        sleep(1)
        s.sendall(b'E')
        sleep(1)
        s.sendall(b'T')
        sleep(1)
        s.sendall(b' ')
        sleep(1)
        s.sendall(b'/')
        sleep(1)
        s.sendall(b' ')
        sleep(1)
        s.sendall(b'H')
        sleep(1)
        s.sendall(b'T')
        sleep(1)
        s.sendall(b'T')
        sleep(1)
        s.sendall(b'P')
        sleep(1)
        s.sendall(b'/')
        sleep(1)
        s.sendall(b'1')
        sleep(1)
        s.sendall(b'.')
        sleep(1)
        s.sendall(b'1')
        sleep(1)
        s.sendall(b'\r\n')
        sleep(1)
        s.sendall(b'Host: cs.muic.mahidol.ac.th\r\nConnection: close\r\n\r\n')
        while data := s.recv(1024):
            print(data)
            
def test_6():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('cs.muic.mahidol.ac.th', 80))
        s.sendall(b'GE')
        sleep(0.01)
        s.sendall(b'T ')
        sleep(2)
        s.sendall(b'/ HT')
        sleep(1.5)
        s.sendall(b'TP/1')
        sleep(3.3)
        s.sendall(b'.1\r')
        sleep(2.6)
        s.sendall(b'\n')
        sleep(1)
        s.sendall(b'Host: cs.muic.mahidol.ac')
        sleep(1.1)
        s.sendall(b'.th\r\n')
        sleep(0.1)
        s.sendall(b'Connection: close\r\n')
        sleep(10)
        s.sendall(b'\r\n\r\n')
        while data := s.recv(1024):
            print(data)

def test_7():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('192.168.1.11', 8080))
        s.endall(b'HEAD /index.html HTTP/1.1\r\n\r\n')
        while data := s.recv(1024):
            print(data)

def iltest_1():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('192.168.1.11', 8080))
        s.sendall(b'HEAD /zzz.html HTTP/1.1\r\nHost: cs.muic.mahidol.ac.th\r\nConnection: close\r\n\r\n')
        while data := s.recv(1024):
            print(data)

def iltest_2():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('192.168.1.11', 8080))
        s.sendall(b'\r\n\r\n')
        while data := s.recv(1024):
            print(data)
            
def iltest_3():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:

        s.connect(('cs.muic.mahidol.ac.th', 80))
        s.sendall(b'GET /index.html HTTPS/1.1\r\nHost: cs.muic.mahidol.ac.th\r\nConnection: close\r\n\r\n')
        while data := s.recv(1024):
            print(data)

def iltest_4():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('192.168.1.11', 8080))
        s.sendall(b'Get /index.html HTTP/99\r\n\r\n')
        while data := s.recv(1024):
            print(data)
            
            
def iltest_6():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:

        s.connect(('cs.muic.mahidol.ac.th', 80))
        s.sendall(b'GET /INDEX.HTML HTTP/1.1\r\nHOST: CS.MUIC.MAHIDOL.AC.TH\r\nCONNECTION: CLOSE\r\n\r\n')
        while data := s.recv(1024):
            print(data)

def iltest_5():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:

        s.connect(('cs.muic.mahidol.ac.th', 80))
        s.sendall(b'get /index.html HTTP/1.1\r\nHost: cs.muic.mahidol.ac.th\r\nConnection: close\r\n\r\n')

        while data := s.recv(1024):
            print(data)

def iltest_7():
    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect(('192.168.1.11', 8080))
        s.sendall(b'POST \index.html HTTP/1.1\r\n\r\n')
        while data := s.recv(1024):
            print(data)

# test_1()
# print()
# test_2()
# print()
# test_3()
# print()
# test_4()
# print()
# test_5()
# print()
# test_6()
# print()
# test_7()
# print()
# iltest_1()
# print()
# iltest_2()
# print()
# iltest_3()
# print()
# iltest_4()
# print()
# iltest_5()
# print()
# iltest_6()
# print()
# iltest_7()
# print()