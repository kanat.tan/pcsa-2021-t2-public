import http.client

# legit test 1
conn = http.client.HTTPConnection(host="cs.muic.mahidol.ac.th", port=80)

conn.set_debuglevel(1)  # print connection-level info (verbose)

headers = {
    'Accept': 'text/plain',
    'Blah': 'Mew mew',
    'Connection': 'close',
}

conn.request('DELETE', '/', headers=headers)  # craft a request + headers
resp = conn.getresponse()

print('------------------------------')
print(f"status={resp.status}, reason={resp.reason}")
print(f"headers={resp.headers}")
print("Body:")
print(resp.read())

# legit test 2
conn = http.client.HTTPConnection(host="cs.muic.mahidol.ac.th", port=80)

conn.set_debuglevel(1)  # print connection-level info (verbose)

headers = {
    'Accept': 'text/plain',
    'Blah': 'Mew mew',
    'Connection': 'close',
}

conn.request('GET', '/~ktangwon/pcsa-p2/', headers=headers)  # craft a request + headers
resp = conn.getresponse()

print('------------------------------')
print(f"status={resp.status}, reason={resp.reason}")
print(f"headers={resp.headers}")
print("Body:")
print(resp.read())

# legit test 3
conn = http.client.HTTPConnection(host="cs.muic.mahidol.ac.th", port=80)

conn.set_debuglevel(1)  # print connection-level info (verbose)

headers = {}

conn.request('GET', '/', headers=headers)  # craft a request + headers
resp = conn.getresponse()

print('------------------------------')
print(f"status={resp.status}, reason={resp.reason}")
print(f"headers={resp.headers}")
print("Body:")
print(resp.read())

import socket as sk

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s2:
    # illegal test 2
    s2.connect(('cs.muic.mahidol.ac.th', 80))  # (host: str, port: int)

    s2.sendall(b"GOT / HTTP/1.1\r\nHost: cs.muic.mahidol.ac.th\r\nConnection: close\r\n")
    
    while data2 := s2.recv(1024):
        print(f'{data2}\n')


    # illegal test 3
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s3:
    s3.connect(('cs.muic.mahidol.ac.th', 80))  # (host: str, port: int)

    s3.sendall(b"GET / HTTP/4.3\r\nHost: cs.muic.mahidol.ac.th\r\nConnection: close\r\n")


    while data3 := s3.recv(1024):
        print(f'{data3}\n')


    # illegal test 4
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s4:
    s4.connect(('cs.muic.mahidol.ac.th', 80))  # (host: str, port: int)

    s4.sendall(b"GET / HTTP/1.1\r\nConnection: close\r\n")

    while data4 := s4.recv(1024):
        print(f'{data4}\n')

