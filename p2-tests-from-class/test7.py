import socket as sk
from time import sleep

# Legal 1 (but bad)
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b'HE')
    sleep(0.25)
    s.sendall(b'AD /cat.jp')
    sleep(1)
    s.sendall(b'g HTTP/1.1')
    sleep(3)
    s.sendall(b' Host: res')
    sleep(0.25)
    s.sendall(b'd\r\n')
    sleep(0.25)
    s.sendall(b'\r')
    sleep(2)
    s.sendall(b'\n')

    while data := s.recv(1024):
        print(data)

# Legal 2
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GET /cat.jpg HTTP/1.1\r\nHost: laptop1\r\nConnection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

# Legal 3
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GET /folder1/index.html HTTP/1.1\r\nHost: laptop1\r\nConnection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

# Legal 4
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GET /index.html HTTP/1.1\r\nHost: laptop1\r\n")
    sleep(50)
    s.sendall(b"Connection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

# Legal 5
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"HEAD /cat.jpg HTTP/1.1\r\nHost: laptop1\r\n")
    s.sendall(b"Connection: close\r\n")
    s.sendall(b"\r\n")

    while data := s.recv(1024):
        print(data)

# Legal 6
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"HEAD /cat.jpg HTTP/1.1\r\n\r\n")

    while data := s.recv(1024):
        print(data)

# Legal 7
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GET /cat.jpg HTTP/1.1\r\nConnection: Keep-Alive\r\n\r\n")

    while data := s.recv(1024):
        print(data)

# Illegal 1
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GEET /cat.jpg HTTP/1.1\r\nHost: laptop1\r\nConnection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

# Illegal 2
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GET /cat.jpg HTTP/2.1\r\nHost: laptop1\r\nConnection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

# Illegal 3
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GET HEAD /cat.jpg HTTP/1.1\r\nHost: laptop1\r\nConnection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)


# Illegal 4
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GET / HTTP/1.1\r\nHost: laptop1\r\nConnection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)

# Illegal 5
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"GET / HTTP/1.1\r\nHost: laptop1\r\nConnection: close\r\n\r\n")

    while data := s.recv(1024):
        print(data)


# Illegal 6
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"HEAD /cat.jpg HTTP/1.1\r\nHost: laptop1\r\nConnection: close\r\n\r")

    while data := s.recv(1024):
        print(data)

# Illegal 7
with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('localhost',54321))

    s.sendall(b"HEAD HTTP/1.1\r\nHost: laptop1\r\n")
    sleep(100)

    while data := s.recv(1024):
        print(data)