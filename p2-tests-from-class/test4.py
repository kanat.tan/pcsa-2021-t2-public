import socket as sk
from time import sleep

print("Enter address: ", end='')
addr = input()
print("Enter port: ", end='')
try:
    port = int(input())
except:
    print('Not a number inputted')
    exit(0)

print(f'Testing address {addr} at port {port}')

try: # Note Test 1 and 2 are like testing two things at once
    # Test 1
    print("Test 1") # Test if everything is just working fine (GET) + testing if connection does not close instantly
    print("Enter a path for your website: ", end='') # Maybe if you want to test other path
    t1Loc = input()
    s1 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s1.connect((addr, port))
    try:
        s1.settimeout(5.0) # Since no Connection: close is passed, should die after 5 seconds
        s1.sendall(b'GET ' + t1Loc.encode('UTF-8') + b'  HTTP/1.1\r\n'
            + b'Host: ' + addr.encode('UTF-8')
            + b'\r\n\r\n')
        while data := s1.recv(1024):
            print(data)
    except: pass
    s1.shutdown(sk.SHUT_RDWR)
    s1.close()
    print("End of Test 1")
    print("Test 2") # Test if everything is just working fine (HEAD) + Connection close
    print("Enter a path for your website: ", end='')
    t2Loc = input()
    s2 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s2.connect((addr, port))
    try:
        s2.settimeout(5.0)
        s2.sendall(b'HEAD ' + t2Loc.encode('UTF-8') + b' HTTP/1.1\r\n'
                + b'Host: ' + addr.encode('UTF-8')
                + b'\r\nConnection: close' # I expect an instant close here
                + b'\r\n\r\n')
        while data := s2.recv(1024):
            print(data)
    except: print("Hmm... it took 5 seconds or something else went wrong...")
    s2.shutdown(sk.SHUT_RDWR)
    s2.close()
    print("End of Test 2")
    print("Test 3") # Test by sending sparse data
    print("Enter a path for your website: ", end='')
    t3Loc = input()
    s3 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s3.connect((addr, port))
    try:
        s3.sendall(b'G')
        sleep(0.5)
        s3.sendall(b'ET ')
        sleep(0.1)
        s3.sendall(t3Loc.encode('UTF-8') + b' HT')
        sleep(1)
        s3.sendall(b'TP/1.1\r')
        sleep(0.3)
        s3.sendall(b'\n')
        sleep(3)
        s3.sendall(b'Host: ')
        s3.sendall(addr[:len(addr)//2].encode('UTF-8'))
        sleep(0.1)
        if (len(addr)//2 < len(addr)): s3.sendall(addr[len(addr)//2].encode('UTF-8'))
        sleep(1)
        s3.sendall(addr[len(addr)//2+1:].encode('UTF-8'))
        s3.sendall(b'\r\nConnection: close')
        sleep(15)
        s3.sendall(b'\r\n')
        sleep(5)
        s3.sendall(b'\r')
        sleep(3)
        s3.sendall(b'\n')
        while data := s3.recv(1024):
            print(data)
    except: pass
    s3.shutdown(sk.SHUT_RDWR)
    s3.close()
    print("End of Test 3")
    print("Test 4") # Test file typing, we make two request a HEAD to get type then a GET
    s4 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s4.connect((addr, port))
    print("Enter a path for your website: ", end='')
    t4Loc = input()
    try:
        s4.sendall(b'HEAD ' + t4Loc.encode('UTF-8') + b' HTTP/1.1\r\n'
                + b'Host: ' + addr.encode('UTF-8')
                + b'\r\n\r\n')
        theType = ''
        while data := s4.recv(1024):
            print(data)
            if (theType == ''):
                inx = data.find(b"Content-Type: ")
                if (inx > -1):
                    theType = data[inx+len(b"Content-Type: "):]
                    inx2 = theType.find(b"\r")
                    theType = theType[:inx2]
                    break
        s4.sendall(b'GET ' + t4Loc.encode('UTF-8') + b' HTTP/1.1\r\n'
                 + b'Host: ' + addr.encode('UTF-8')
                 + b'\r\nContent-Type: ' + theType
                 + b'\r\nConnection: close\r\n\r\n')
        while data := s4.recv(1024):
            print(data)
    except Exception as e: print(e)
    s4.shutdown(sk.SHUT_RDWR)
    s4.close()
    print("End of Test 4")
    print("Test 5") # Test when final \r\n is never sent
    print("Enter a path for your website: ", end='')
    t5Loc = input()
    s5 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s5.connect((addr, port))
    try:
        s5.settimeout(5.0)
        s5.sendall(b'HEAD ' + t5Loc.encode('UTF-8') + b' HTTP/1.1\r\n'
                + b'Host: ' + addr.encode('UTF-8')
                + b'\r\nConnection: close'
                + b'\r\n')
        while data := s5.recv(1024):
            print(data)
    except: pass # Good sign if connection closes after 5 seconds despite of Connection: close
    s5.shutdown(sk.SHUT_RDWR)
    s5.close()
    print("End of Test 5")
    print("Test 6") # Bad header
    print("Enter a path for your website: ", end='')
    t6Loc = input()
    s6 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s6.connect((addr, port))
    try:
        s6.settimeout(5.0)
        s6.sendall(b'G  E T ' + t1Loc.encode('UTF-8') + b'  HTTP/1.1\r\n'
            + b'Host: ' + addr.encode('UTF-8')
            + b'\r\n\r\n')
        while data := s6.recv(1024):
            print(data)
    except: pass
    s6.shutdown(sk.SHUT_RDWR)
    s6.close()
    print("End of Test 6")
    print("Test 7") # Check whitespaces
    print("Enter a path for your website: ", end='')
    t7Loc = input()
    s7 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s7.connect((addr, port))
    try:
        s7.settimeout(5.0)
        s7.sendall(b'GET ' + t7Loc.encode('UTF-8') + b'HTTP/1.1\r\n'
            + b'Host:   ' + addr.encode('UTF-8')
            + b'\r\n\r\n')
        while data := s7.recv(1024):
            print(data)
    except: pass
    s7.shutdown(sk.SHUT_RDWR)
    s7.close()
    print("End of Test 7")
    print("Test 8") # Different Content-Type
    print("Location used: /")
    s8 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s8.connect((addr, port))
    try:
        s8.settimeout(5.0)
        s8.sendall(b'GET / HTTP/1.1\r\n'
            + b'Host: ' + addr.encode('UTF-8')
            + b'Content-Type: application/json\r\n'
            + b'\r\n\r\n')
        while data := s8.recv(1024):
            print(data)
    except: pass
    s8.shutdown(sk.SHUT_RDWR)
    s8.close()
    print("End of Test 8")
    print("Test 9") # A random unrecognized header
    print("Enter a path for your website: ", end='')
    t9Loc = input()
    s9 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s9.connect((addr, port))
    try:
        s9.settimeout(5.0)
        s9.sendall(b'HEAD ' + t9Loc.encode('UTF-8') + b'  HTTP/1.1\r\n'
            + b'Host: ' + addr.encode('UTF-8')
            + b'LalaLand: ok\r\n'
            + b'\r\n\r\n')
        while data := s9.recv(1024):
            print(data)
    except: pass
    s9.shutdown(sk.SHUT_RDWR)
    s9.close()
    print("End of Test 9")
    print("Test 10") # Check if server times out itself
    print("Enter a path for your website: ", end='')
    t10Loc = input()
    s10 = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
    s10.connect((addr, port))
    try:
        s10.sendall(b'GET ' + t1Loc.encode('UTF-8') + b'  HTTP/1.1\r\n'
            + b'Host: ' + addr.encode('UTF-8')
            + b'\r\n\r\n')
        while data := s10.recv(1024):
            print(data)
    except: pass
    s10.shutdown(sk.SHUT_RDWR)
    s10.close()
    print("End of Test 10 and all tests")
except Exception as e:
    print(e)
    print(f'Error connecting to address {addr} at port {port}')

