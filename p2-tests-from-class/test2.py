import http.client
import random
import re
import socket as sk
from time import sleep

# get info
host: str = input("Enter host: ")
port: int = int(input("Enter port: "))

legitReqs = [
    {
        'method': 'GET',
        'url': '/',
        'headers': {
            'Accept': 'text/plain',
            'Connection': 'close',
        },
    },
    {
        'method': 'GET',
        'url': '/',
        'headers': {
            'Accept': 'text/plain',
            'Connection': 'close',
            'Hello': 'World',
        },
    },
    {
        'method': 'GET',
        'url': '/',
        'headers': {
            'Host': host,
        },
    },
    {
        'method': 'GET',
        'url': '/',
        'headers': {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0'
        },
    },
    {
        'method': 'HEAD',
        'url': '/',
        'headers': {
            'Accept': 'text/plain',
            'Connection': 'close',
        },
    },
    {
        'method': 'HEAD',
        'url': '/',
        'headers': {
            'Accept': 'text/plain',
            'Connection': 'close',
            'Hello': 'World',
        },
    },
    {
        'method': 'HEAD',
        'url': '/',
        'headers': {
            'Host': host,
        },
    },
]

illegalReqs = [
    {
        'method': 'WHAT!?',
        'url': '/',
        'headers': {
            'Accept': 'text/plain',
            'Connection': 'close',
        },
    },
    {
        'method': 'GET',
        'url': 'mywebsite.com',
        'headers': {
            'Accept': 'text/plain',
            'Connection': 'close',
            'Hello': 'World',
        },
    },
    {
        'method': 'GETT',
        'url': '/',
        'headers': {
            'Host': host,
            'Accept': 'plain/text\r\n\n\n\r'
        },
    },
    {
        'method': 'HEADER',
        'url': '/',
        'headers': {
            'User-Agent': 'Google Chrome'
        },
    },
    {
        'method': 'HEAD',
        'url': 'mywebsite.com',
        'headers': {
            'Accept': 'text/plain',
        },
    },
    {
        'method': 'HEAD',
        'url': '/',
        'headers': {
            'Accept': 'text/plain',
            'Connection': 'close',
            '\nHello\r': '\r\nWorld\r',
        },
    },
    {
        'method': 'BROKE',
        'url': '/',
        'headers': {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0'
        },
    },
]


def send_request(req: dict):  # use httpClient to send while request
    conn = http.client.HTTPConnection(host=host, port=port)
    conn.request(req['method'], req['url'], headers=req['headers'])
    resp = conn.getresponse()

    print('------------------------------')
    print(f"status={resp.status}, reason={resp.reason}")
    print(f"headers={resp.headers}")
    print("Body:")
    print(resp.read())


# split request into parts & sleep between sending each part
def send_splitted_request(req: dict):
    headerStr: bytes = f"{req['method']} {req['url']} HTTP/1.1\r\n".encode('UTF-8') + ''.join(
        [f'{key}: {value}\r\n' for key, value in req['headers'].items()]).encode('UTF-8') + b'\r\n'
    numSplits = random.randint(2, 6)
    splitted = [headerStr[i:i+numSplits]
                for i in range(0, len(headerStr), numSplits)]

    with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
        s.connect((host, port))

        for part in splitted:
            s.sendall(part)
            sleep(random.uniform(0.25, 0.75))

        print('------------------------------')

        while data := s.recv(1024):
            print(data.decode('UTF-8'))


for legitReq in legitReqs:
    send_request(legitReq)
    send_splitted_request(legitReq)

for illegalReq in illegalReqs:
    send_request(illegalReq)
    send_splitted_request(illegalReq)
