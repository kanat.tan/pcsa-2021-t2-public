from http.server import HTTPServer, BaseHTTPRequestHandler

def real_my_magic(n: int) -> int:
    return n**2 + 1

class MyServerHandler(BaseHTTPRequestHandler):
    def do_POST(self):
        # handle a post request
        content_length = int(self.headers.get('content-length', '0').strip())
        request = self.rfile.read(content_length)

        # decode n
        n = int(request.strip())

        # call the actual code to run it
        answer = real_my_magic(n)

        # encode the response
        encoded = str(answer)

        # send it back
        self.send_response(200)
        self.end_headers()  # write HTTP headers
        self.wfile.write(bytes(f'{encoded}\r\n', 'utf-8')) # write body


web_server = HTTPServer(('', 8090), MyServerHandler)

try:
    web_server.serve_forever()
except KeyboardInterrupt:
    print("Exiting...")

web_server.server_close()
