from http.client import HTTPConnection

TARGET = "localhost:8090"

def my_magic(n: int) -> int:   ## my_magic(4)
    # the actual computation is run remotely
    # encode this n 
    encoded = str(n)   ## "4"
    # send over the request
    conn = HTTPConnection(TARGET)
    conn.request('POST', '/', body=encoded)  # send body="4"
    # wait for response
    resp = conn.getresponse().read() # expect: "17 "
    # decode the response & return
    return int(resp.strip())  # return 17


if __name__ == "__main__":
    r = my_magic(4)
    print(r)
