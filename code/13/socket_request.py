import socket as sk
from time import sleep 

with sk.socket(sk.AF_INET, sk.SOCK_STREAM) as s:
    s.connect(('cs.muic.mahidol.ac.th', 80))  # (host: str, port: int)

    # s.sendall(b"GET / HTTP/1.1\r\nHost: cs.muic.mahidol.ac.th\r\n\r\n")
    # 'G'
    s.sendall(b'G')
    sleep(0.25)
    # 'ET /'
    s.sendall(b'ET /')
    sleep(1)
    # ' HTTP/1.1\r\nHost: cs.muic.'
    s.sendall(b' HTTP/1.1\r\nHost: cs.muic.')
    sleep(5)
    # 'mahidol.ac.th\r\n\r'
    s.sendall(b'mahidol.ac.th\r\n\r')
    sleep(0.5)
    # '\n'
    s.sendall(b'\n')

    while data := s.recv(1024):
        print(data)
