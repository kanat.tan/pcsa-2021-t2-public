# Using http.client to talk to an HTTP server

import http.client

conn = http.client.HTTPConnection(host="cs.muic.mahidol.ac.th", port=80)

conn.set_debuglevel(1)  # print connection-level info (verbose)

headers = {
    'Accept': 'text/plain',
    'Blah': 'Mew mew',
    'Connection': 'close',
}

conn.request('GET', '/', headers=headers)  # craft a request + headers
resp = conn.getresponse()

print('------------------------------')
print(f"status={resp.status}, reason={resp.reason}")
print(f"headers={resp.headers}")
print("Body:")
print(resp.read())

