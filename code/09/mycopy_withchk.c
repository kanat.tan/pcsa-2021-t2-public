#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>

#ifndef BUFSIZE         /* a trick: to allow overriding BUFSIZE */
#define BUFSIZE 1024
#endif

#define PANIC(args...) fprintf(stderr, args)

int main(int argc, char* argv[]) {
    int inputFileDescr, outputFileDescr;

    char buf[BUFSIZE];

    if (argc != 3) {
        printf("%s <old file> <new file>\n", argv[0]);
        exit(-1);
    }

    /* Open source and destination files */

    inputFileDescr = open(argv[1], O_RDONLY);

    if (inputFileDescr == -1)
        PANIC("ERROR: Cannot open src file %s\n", argv[1]);

    int dstOpenFlags = O_CREAT | O_WRONLY | O_TRUNC;
    int dstFilePerms = 0644; /* rw-r--r-- */

    outputFileDescr = open(argv[2], dstOpenFlags, dstFilePerms);

    if (outputFileDescr == -1)
        PANIC("ERROR: Cannot open dst file %s\n", argv[2]);

    /* Transfer the bytes until we encounter the end of the file */
    ssize_t numRead;

    while ((numRead = read(inputFileDescr, buf, BUFSIZE)) > 0) {
        ssize_t numWritten = 0;

        while (numWritten < numRead) {
            ssize_t bytesWritten = write(outputFileDescr, 
                                         buf + numWritten, 
                                         numRead - numWritten);
            if (bytesWritten < 0)
                PANIC("ERROR: Couldn't write the buffer!\n");

            numWritten += bytesWritten;
        }
    }

    if (numRead == -1)
        PANIC("ERROR: Read failure\n");

    if (close(inputFileDescr) == -1) 
        PANIC("ERROR: Failed to close input");

    if (close(outputFileDescr) == -1) 
        PANIC("ERROR: Failed to close output");

    return 0;
}
