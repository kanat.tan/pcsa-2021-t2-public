#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFSIZE 16

// Goal: copy the contents from <srcFile> to <dstFile>
//   ./mycopy_simple <srcFile> <dstFile>
// Use the lowest-level I/O API
int main(int argc, char* argv[]) {
    int inputFd, outputFd;
    char buf[BUFSIZE];
    /* Open source and destination file */
    inputFd = open(argv[1], O_RDONLY);
    int openFlags = O_CREAT | O_WRONLY | O_TRUNC;
    int dstPerms = 0644; // rw-r--r--
    outputFd = open(argv[2], openFlags, dstPerms);

    // Repeat until done
    //   read from A 
    //   write to B
    ssize_t numRead; // the number of bytes read
    while ((numRead = read(inputFd, buf, BUFSIZE)) > 0) {
        write(outputFd, buf, numRead);
    }

    /* Close both files */
    close(inputFd);
    close(outputFd);

    return 0;
}
