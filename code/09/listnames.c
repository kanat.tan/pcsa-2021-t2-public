#include<sys/types.h>
#include<dirent.h>
#include<stdlib.h>
#include<stdio.h>

int main(int argc, char* argv[]) {
    DIR *cwd = opendir(".");
    
    if (cwd == NULL) exit(-1);

    struct dirent* entry;

    while ((entry = readdir(cwd)) != NULL) {
        char *attr = "other";
        if (entry->d_type == DT_DIR) attr = "dir";
        if (entry->d_type == DT_REG) attr = "file";
        if (entry->d_type == DT_BLK) attr = "block";
        printf("%s [%s]\n", entry->d_name, attr);
    }

    closedir(cwd);
    return 0;
}
