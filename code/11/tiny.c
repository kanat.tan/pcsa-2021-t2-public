#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include "pcsa_net.h"


/* Rather arbitrary. In real life, be careful with buffer overflow */
#define MAXBUF 1024  

typedef struct sockaddr SA;
void respond_helloworld(int connFd) {
    char buf[MAXBUF];
    char *msg = "<h1>Hello, World</h1> This is a test.";

    sprintf(buf, "HTTP/1.1 200 OK\r\n"
            "Server: Tiny\r\n"
            "Connection: close\r\n"
            "Content-length: %lu\r\n"
            "Content-type: text/html\r\n\r\n", strlen(msg));

    write_all(connFd, buf, strlen(buf));
    write_all(connFd, msg, strlen(msg));
}
void serve_http(int connFd) {
    // First line: a request line
    // Request line:  METHOD URI VERSION
    // Example     :  GET / HTTP/1.0
    char buf[MAXBUF];
    if (!read_line(connFd, buf, MAXBUF))
        return ; /* quit if read_line failed */

    printf("LOG %s\n", buf);

    // strtok <- not thread safe (don't use in P2)
    // sscanf
    char method[MAXBUF], uri[MAXBUF], version[MAXBUF];
    sscanf(buf, "%s %s %s", method, uri, version);

    /* Drain the remaining lines in the request */
    while (read_line(connFd, buf, MAXBUF) > 0) {
        if (strcmp(buf, "\r\n") == 0) break;
    }

    if (strcasecmp(method, "GET")==0 && strcmp(uri, "/")==0) {
        // There is a request of the form GET / {SOME_VERSION}
        // We should respond
        respond_helloworld(connFd);
    }
}

int main(int argc, char* argv[]) {
    int listenFd = open_listenfd(argv[1]);

    for (;;) {
        struct sockaddr_storage clientAddr;
        socklen_t clientLen = sizeof(struct sockaddr);

        int connFd = accept(listenFd, (SA *) &clientAddr, &clientLen);
        if (connFd < 0) { fprintf(stderr, "Failed to accept\n"); continue; }

        char hostBuf[MAXBUF], svcBuf[MAXBUF];
        if (getnameinfo((SA *) &clientAddr, clientLen, 
                        hostBuf, MAXBUF, svcBuf, MAXBUF, 0)==0) 
            printf("Connection from %s:%s\n", hostBuf, svcBuf);
        else
            printf("Connection from ?UNKNOWN?\n");

        // service the client here (pass to it connFd)
        serve_http(connFd);
        close(connFd);
    }

    return 0;
}
