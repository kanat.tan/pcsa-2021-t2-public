#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include "pcsa_net.h"


#define BUFSIZE 255

typedef struct sockaddr SA;

void echo_logic(int connFd) {
    ssize_t bytesRead;
    char buf[BUFSIZE];

    // read everything connFd & write it back to connFd
    while ((bytesRead = read(connFd, buf, BUFSIZE)) > 0) {
        printf("DEBUG: Read %ld bytes\n", bytesRead);

//        write(connFd, buf, bytesRead); // potential bug: short counts
        ssize_t numToWrite = bytesRead;
        char *writeBuf = buf;

        while (numToWrite > 0) {
            ssize_t numWritten = write(connFd, writeBuf, numToWrite);
            if (numWritten < 0) { printf("ERROR: Couldn't write\n"); break; }
            numToWrite -= numWritten;
            writeBuf += numWritten;
        }
    }
    printf("DEBUG: Connection was closed by the client");
}

// ./echo_server 3000
int main(int argc, char* argv[]) {
    int listenFd = open_listenfd(argv[1]); 


    for (;;) {
        struct sockaddr_storage clientAddr;
        socklen_t clientLen = sizeof(struct sockaddr_storage);

        int connFd = accept(listenFd, (SA *) &clientAddr, &clientLen);

        char hostBuf[BUFSIZE], svcBuf[BUFSIZE];
        if (getnameinfo((SA *) &clientAddr, clientLen, hostBuf, BUFSIZE, svcBuf, BUFSIZE, 0) == 0) {
            printf("Connection from %s:%s\n", hostBuf, svcBuf); 
        }
        else {
            printf("Connection from UNKNOWN.");
        }

        echo_logic(connFd);

        close(connFd);
    }

    return 0;
}
