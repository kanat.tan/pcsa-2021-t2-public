#include <thread>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include "simple_work_queue.hpp"

using namespace std;

struct {
    work_queue work_q;
} shared;

void do_work() {
    for (;;) { // Loop forever until told to quit
        // Grab a job & work on it
        long w;
        if (shared.work_q.remove_job(&w)) {
            if (w < 0) break; // negative term means quit
            // got the job
            printf("%% Thinking for a bit (%lds)\n", w); fflush(stdout);
            sleep(w);
            printf("%% Done thinking\n"); fflush(stdout);
        }
        else {
//            continue; // LET'S DISCUSS!
            sleep(0); // SLEEP INSTEAD OF SPINNING (and wasting my CPU)
        }
    }
}

int main(int argc, char* argv[]) {

    // What is work? 
    //    Work is represented as a number w and the actual work
    //    is sleeping for w seconds.
    //
    thread worker(do_work);
    
    for (;;) {
        long w;
        printf(">>> ");fflush(stdout);
        scanf("%ld", &w);

        shared.work_q.add_job(w);
        if (w < 0) { printf("Exiting....\n"); break; }
    }
    worker.join();
    return 0;
}
